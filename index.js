// console.log("Hello Batch243")

// Function
	/* syntax:
		function functionName(){
			code block (statement)
		}
	*/

function printName(){
	console.log("My name is John.");
	console.log("My lat name is Dela Cruz.")
}
// Let's invoke the function that we declared
printName();
printName();


// Function Declaration Vs Function Expression

	//Function Declaration - use keyword function and adding function name

		declaredFunction();

		function declaredFunction(){
			console.log("Hello World from declaredFunction")
		}


	// Function Expression - a function stored in a variable

		// Anonymous function - a function w/o a name.
			
			// variableFunction();
			let variableFunction = function(){
				console.log("Hello from variableFunction!")
			}
			variableFunction();

			let funcExpression = function funName(){
				console.log("Hello from funcExpression")
			}
			funcExpression();

	// you can reassign declared function and expression to new anonymous function.

			declaredFunction= function(){
				console.log("updated declaredFunction")
			}

			declaredFunction();

			funcExpression= function(){
				console.log("updated funcExpression")
			}

			funcExpression();

			// Function experssion using const keyword
			const constantFunc = function(){
				console.log("Initialized with const!")
			}
			constantFunc();

			/*constantFunc = function(){
				console.log("cannot be reassigned!")
			}
			constantFunc(); */


// Function Scoping - accesibility of variable within our program.

	/* JS variable has 3 types of scop:
		1. local/block scop
		2. global scope
		3. function scope
	*/
		// local Function
		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}

		
		let globalVar = "Mr. Worldwide";

		
		function showNames(){
			let functionLet = "Jane";
			const functionConst = "John";

			console.log(functionConst);
			console.log(functionLet);
		}

		showNames();
		

		// The functionConst and functionLet cannot be accessed outside of the function they were declared.
		// console.log(functionLet);
		// console.log(functionConst);


// Nested Function

		function myNewFunction(){
			let name = "Jane";
			console.log(name);
			function nestedFunction(){
				let nestedName = "John";

				console.log(nestedName);
				console.log(name)
			}
			nestedFunction();
		}

		myNewFunction();


	// Function & globla Scoped Variable
		// GLobal Scoped Variable
		let globalName="Alexandro";

		function myNewfunction2(){
			let nameInside="Renz";
			
			console.log(globalName);
			console.log(nameInside);
		}

		myNewfunction2();


// Alert
	alert("Hello World!"); //this will run immediately when page loads

	function showsSampleAlert(){
		alert("Hello, User!");
	}

	showsSampleAlert();

// Prompt 
	let samplePrompt = prompt("Enter your name: ");
	console.log(samplePrompt);
	console.log(typeof samplePrompt);

	let sampleNullPrompt = prompt('Dont Enter Anything');
	console.log(sampleNullPrompt);
	console.log(typeof sampleNullPrompt);

	function printWelcomeMessages(){
		let firstName = prompt("Enter your First Name: ");
		let lastName = prompt("Enter your last Name: ");

		console.log("Hello, " + firstName + " " + lastName + "!");
	}

	printWelcomeMessages();


// Function Naming

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];

		console.log(course);
	}

	getCourses();